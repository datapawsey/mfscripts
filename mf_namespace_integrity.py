#!/usr/bin/env python3

import os
import sys
import time
import getpass
import mfclient

if __name__ == '__main__':

    token=os.environ['MFLUX_TOKEN']
#    token=None

# enforce system:manager login 
    user = "manager"
    domain = "system"
    try:
        mf_client = mfclient.mf_client('https', '443', 'data.pawsey.org.au', domain=domain)
        if token is not None:
            mf_client.login(token=token)
        else:
            password = getpass.getpass("Enter %s:%s password: " % (domain, user))
            mf_client.login(domain, user, password)
    except Exception as e:
        print("Login failed in: %s" % str(e))
        exit(-1)

# ---- CUSTOM SCRIPT STARTS HERE
# CURRENT - simple check of description field for the pawsey project code

    result = mf_client.aterm_run("asset.namespace.list :namespace /projects")
#    mf_client.xml_print(result)

    try:
        for item in result.iter("namespace"):
            if item.text is not None:
                try:
                    reply = mf_client.aterm_run('asset.namespace.describe :namespace "/projects/%s"' % item.text)
#                    mf_client.xml_print(reply)

                    elem = reply.find(".//namespace/description")
                    if elem is not None:
                        print("[%s] -> [%s]" % (elem.text, item.text))

                except Exception as e:
                    print("Fail")
                    print("  > %s" % str(e))

    except KeyboardInterrupt:
        print("User interrupted")
        exit(-1)

    print("Done")
