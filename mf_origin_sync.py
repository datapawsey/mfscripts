#!/usr/bin/env python3

import os
import getpass
import mfclient

import urllib3
import json

# boilerplate for running mediaflux admin commands 

######
# main
######

domain = 'system'

# DEBUG - set special=None to do all, otherwise will do just the named project
special = None
#special = 'APPF'

if __name__ == '__main__':

    http = urllib3.PoolManager()

#regen> secure.identity.token.create :role -type user "system:manager" :min-token-length 16
    try:
        mflux_token = os.environ['MFLUX_TOKEN']
        mf_client = mfclient.mf_client('https', '443', 'data.pawsey.org.au', domain=domain)
        mf_client.login(token=mflux_token)
    except Exception as e:
        print("MFLUX init error: %s" % str(e))

# regen> via https://portal.pawsey.org.au/origin/portal/account/apitokens
    try:
        origin_token = os.environ['ORIGIN_TOKEN']
        headers = {'Content-Type': 'application/json', 'Accepts': 'application/json', 'Authorization' : f'Bearer %s' % origin_token}
# is there a better token test URL to call?
        url = f'https://portal.pawsey.org.au/origin/api/project' 
        response = http.request('GET', url, headers=headers)
        if response.status != 200:
            raise Exception("authentication failed")

    except Exception as e:
        print("ORIGIN init error: %s" % str(e))

# go ...
    total_count = 0

# check there is a corresponding mediaflux namespace
    xml_reply = mf_client.aterm_run("asset.namespace.list :namespace /projects")
    for elem in xml_reply.findall(".//namespace/namespace"):
        name = elem.text

# DEBUG - override to look at a specific project name
        if special is not None:
            name = special

# get MFLUX project description 
        xml_mflux_desc = mf_client.aterm_run("project.describe :name %s" % name)
#        mf_client.xml_print(xml_mflux_desc)

# get the project note => pawsey code
        try:
            xml_elem = xml_mflux_desc.find(".//note")
            origin_name = xml_elem.text.lower()
# skip internal/inactive project
            if origin_name.startswith('flagged'):
                print("[%s] -> SKIP: %s" % (name, origin_name))
                continue
            if origin_name.startswith('internal'):
                print("[%s] -> SKIP: %s" % (name, origin_name))
                continue

        except Exception as e:
            print("MFLUX project [%s] has no valid pawsey code" % name)
            continue

        print("Syncing mflux=%s with origin=%s ..." % (name, origin_name))

        try:
# check project exists
# storage seems to always return 404 if project  doesn't exist
            url = f'https://portal.pawsey.org.au/origin/api/project/%s/storage' % origin_name
            response = http.request('GET', url, headers=headers)

            if response.status == 200:
                hash_storage = json.loads(response.data)
# get membership
# NB: membership response seems to be always 200 even if project doesn't exist ... 
                url = f'https://portal.pawsey.org.au/origin/api/project/%s/memberswithuser' % origin_name
                response = http.request('GET', url, headers=headers)
                hash_members = json.loads(response.data)

#                print("%r" % hash_members)

# FIXME - this will fail when the user is ADMIN in origin, but only readwrite in MFLUX
# Origin doesn't have readonly ...


# NEW STYLE - more finegrained foreach by MFLUX role
                for user in hash_members:

                    uid = user['researcher']['uid']
                    actor = 'ivec:%s' % uid

                    origin_admin = user['admin'] or user['pi']

#                    print("searching for: %s" % actor)

                    mflux_found = False

                    for xml_elem in xml_mflux_desc.findall('.//*role'):
                        if xml_elem is not None:
#                            print("%r,%r" % (xml_elem.tag, xml_elem.text))

                            if 'admin' in xml_elem.text:
                                mflux_admin = True
                            else:
                                mflux_admin = False

                            for xml_child in xml_elem:
#                                print(xml_child.text)

# handling for mflux project members that are not in the origin project?
# can warn, but adding might be tricky due to origin invite process
                                if xml_child.text == actor:
                                    if origin_admin == mflux_admin:
#                                        print("correct match")
                                        mflux_found = True
                                        break
#                                    else:
#                                        print("WARN: extra role in mflux?")

# if the equivalent role + user in origin was not found in mflux, add
                    if mflux_found is False:
                        if origin_admin is True:
                            print("[%s] - adding %s - administer " % (name, actor))
                            mf_client.aterm_run('project.grant :name %s :administer "%s"' % (name, actor))
                        else:
                            print("[%s] - adding %s - readwrite " % (name, actor))
                            mf_client.aterm_run('project.grant :name %s :readwrite "%s"' % (name, actor))

# DEBUG - drop out after we're done
                if special is not None:
                    exit(0)

            else:
                print("No such project %s in origin, skipping..." % origin_name)

        except Exception as e:
            print(str(e))


