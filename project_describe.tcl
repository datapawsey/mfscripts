# return detailed project information
#

set ns_projects "/projects"

set xdoc_result [xnew]

# I know it's an insane way to check for the default '*' but that's the madness TCL drives you to
if { [string length $name] > 1 } {
    if [ catch { asset.namespace.list :namespace $ns_projects :prefix $name } xml ] {
        return [xnew :error "Failed to list project namespaces."]
    }
} else {
    if [ catch { asset.namespace.list :namespace $ns_projects } xml ] {
        return [xnew :error "Failed to list project namespaces."]
    }
}

# NB: have to use an array and not a list as TCL splits lists based on spaces
set project_count [xcount "namespace/namespace" $xml]

# individual project iteration
for { set idx 0 } { $idx < $project_count } { incr idx } {

    set project [xvalue "namespace/namespace\[$idx\]" $xml]

    set namespace "$ns_projects/$project"

# query project namespace details
if [ catch { asset.namespace.describe :namespace $namespace :list-contents false } xml_namespace ] {
    set quota "0"
    set usage "0"
    set note "Missing namespace description"
} else {
# FIXME - new storage portal can't handle values with units
#    set quota [xvalue namespace/quota/allocation/@h $xml_namespace]
#    set usage [xvalue namespace/quota/used/@h $xml_namespace]
    set quota [xvalue namespace/quota/allocation $xml_namespace]
    set usage [xvalue namespace/quota/used $xml_namespace]
    set note [xvalue namespace/description $xml_namespace]
}

# query authorization details
if [ catch { authorization.role.list :namespace $project } xml_roles ] {
	return [xnew :error "Not a project: no matching role namespace"]
}

# extract description (this is the ACL description)
if [ catch { authorization.role.namespace.describe :namespace $project } xml_rns ] {
    set description "No permission to view"
} else {
# FIXME - if you have no permission to view (eg normal member) it lands here anyway with a blank payload
    set description [xvalue namespace/description $xml_rns]
}

# extract role membership
set role_count [xcount "role" $xml_roles]
set xdoc_members [xnew]

# find all users with these roles
for { set j 0 } { $j < $role_count } { incr j } {
	set role_name [xvalue "role\[$j\]" $xml_roles]
	set xdoc_role [xnew :role [xsafe $role_name]]
#puts $role_name
	set actor_list [actors.granted :role -type role "$role_name" :type user]
	set user_list [xvalues actor/@name $actor_list]
	foreach user $user_list {
		set xdoc_user [xnew :actor [xsafe $user]]
		set xdoc_role [xadd $xdoc_role $xdoc_user /role] 
		}
# update member list
	set xdoc_members [xadd $xdoc_members $xdoc_role /members]
}

# build the results document for this project
set xdoc_result [xadd $xdoc_result [xnew :name [xsafe $project]]]
set xdoc_result [xadd $xdoc_result [xnew :note [xsafe $note]]]
set xdoc_result [xadd $xdoc_result [xnew :folder [xsafe $namespace]]]
set xdoc_result [xadd $xdoc_result [xnew :description [xsafe $description]]]
set xdoc_result [xadd $xdoc_result $xdoc_members]
set xdoc_result [xadd $xdoc_result [xnew :quota [xsafe $quota]]]
set xdoc_result [xadd $xdoc_result [xnew :usage [xsafe $usage]]]
}

return $xdoc_result
