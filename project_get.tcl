
# renamed to project.get to avoid conflict with prior project.describe/project.list as there will be an overlap period

proc get_details {name members} {

if [ catch { asset.store.describe :name $name } xml_info ] {
# return empty doc if no attached store ie skips display
	return [xnew] 
} else {

	set xml_rolens [authorization.role.namespace.describe :namespace $name]
	set description [xvalue namespace/description $xml_rolens]
    set administer [xvalue namespace/can-administer $xml_rolens]

# store unmounted?
	set mounted [xvalue store/automount/@mounted $xml_info]

	if {[string equal $mounted "true"]} {

		set quota [xvalue store/mount/max-size $xml_info]
		set usage [xvalue store/mount/size $xml_info]
		set quota_human [xvalue store/mount/max-size/@h $xml_info]
		set usage_human [xvalue store/mount/size/@h $xml_info]

# TODO - if length of human usage/quota is 0 -> probably unquoted -> display unknown or similar
		if {[string length $quota_human] < 1} {
			set quota_human "Unlimited"
		}
		if {[string length $usage_human] < 1} {
			set usage_human "Unknown"
		}
# TODO - compare path to the namespace application settings in /projects 
    set store "todo"

	} else {

# FIXME - strictly, should check that name = file.max.size ... but we're not using any other arg types at present
	set quota [xvalue store/arg $xml_info]
	set quota_human "todo"
    set usage "unknown"
    set usage_human "unknown"
    set mounted "false"

    set store "todo"
	}

}

set xdoc_result [xnew :name [xsafe $name] ]

set xdoc_store [xnew :description [xsafe $description] :administer [xsafe $administer] :quota -bytes \"$quota\" [xsafe $quota_human] :used -bytes \"$usage\" [xsafe $usage_human] :store [xsafe $store] :mounted [xsafe $mounted]]

set xdoc_result [xadd $xdoc_result $xdoc_store /name]

if {[string is true $members]} {
	if [ catch { authorization.role.list :namespace $name } xml_roles ] {
# membership lookup failed
#	puts $xdoc_result
	return $xdoc_result
	}

# extract roles
	set role_count [xcount "role" $xml_roles]

# find all users with these roles
	for { set j 0 } { $j < $role_count } { incr j } {

		set role_name [xvalue "role\[$j\]" $xml_roles]

		set xdoc_role [xnew :role [xsafe $role_name]]

		set actor_list [actors.granted :role -type role "$role_name" :type user]
		set user_list [xvalues actor/@name $actor_list]
		foreach user $user_list {
	
			set xdoc_user [xnew :actor [xsafe $user]]

			set xdoc_role [xadd $xdoc_role $xdoc_user /role] 
			}

		set xdoc_result [xadd $xdoc_result $xdoc_role /name/members]
		}
	}

#puts $xdoc_result

return $xdoc_result
}


set xdoc_total [xnew]

if { $name == "all" } {

# default size is 100
	set result [asset.namespace.list :namespace /projects :size 1000]
	set project_count [xcount "namespace/namespace" $result]
	for { set i 0 } { $i < $project_count } { incr i } {

# skip hidden projects (eg scripts)
		set hidden [xvalue "namespace/namespace\[$i\]/@restricted-visibility" $result]
		if [string equal $hidden "true"] {
			continue
		}

		set name [xvalue "namespace/namespace\[$i\]" $result]

		set xdoc_this [get_details $name $members]
		set xdoc_total [xadd $xdoc_total $xdoc_this]

	}
} else {

# single
	set xdoc_this [get_details $name $members]
	set xdoc_total [xadd $xdoc_total $xdoc_this]
}


return $xdoc_total
