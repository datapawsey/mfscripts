
# destroy roles 
catch {
authorization.role.destroy :role "$name:administer"
} result

catch {
authorization.role.destroy :role "$name:readwrite"
} result

catch {
authorization.role.destroy :role "$name:readonly"
} result

catch {
authorization.role.namespace.destroy :namespace "$name"
} result

# destroy data on disk - can take a long time
if {[string is true $destroy]} {
    asset.namespace.destroy :namespace "/projects/$name"
}

