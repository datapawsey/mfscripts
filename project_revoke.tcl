
# remove project membership

if {[string length $administer] > 1} {
	actor.revoke :role -type role "$name:administer" :name $administer :type user
	}

if {[string length $readwrite] > 1} {
	actor.revoke :role -type role "$name:readwrite" :name $readwrite :type user
	}

if {[string length $readonly] > 1} {
	actor.revoke :role -type role "$name:readonly" :name $readonly :type user
	}

