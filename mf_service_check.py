#!/usr/bin/env python3

import os
import getpass
import mfclient
import urllib.request

# auth - ref:TODO for system-monitor token generation
token = None

# server
protocol = 'https'
port = 443
server = 'data.pawsey.org.au'


if __name__ == '__main__':
# use config to set up mediaflux server to talk to
    try:
        mf_client = mfclient.mf_client(protocol=protocol, port=port, server=server)
    except Exception as e:
        print(str(e))
        exit(-1)

# authenticate
    try:
        if token is not None:
            mf_client.login(token=token)
        else:
            # NB: default domain is system, not ivec (LDAP)
            mf_client.login()
    except Exception as e:
        print(str(e))
        exit(-1)

# service check 1 - web page
    try:
        fp = urllib.request.urlopen("%s://%s" % (protocol, server))
        data = fp.read(15)
        fp.close()
        page = data.decode("utf8")
        if not page == "<!DOCTYPE html>": raise Exception("Server did not return a HTML page")
        print("Server returned HTML page [OK]")

    except Exception as e:
        print(str(e))
        exit(-1)

# service check 2 - banksia content reachability
    try:
        xml1 = mf_client.aterm_run("asset.query :where \"content store 'wood'\" :size 1")
        elem = xml1.find(".//id")
        asset_id = int(elem.text)
        xml2 = mf_client.aterm_run("asset.content.status :id %d" % asset_id)
        elem = xml2.find(".//state")
        state = elem.text
# NB: valid states are "online", "online+offline", "offline"
        if not state.startswith('o'): raise Exception("Banksia status check failed")
        print("Banksia content check [OK]")

    except Exception as e:
        # content state check failed in some way
        print(str(e))
        exit(-1)

