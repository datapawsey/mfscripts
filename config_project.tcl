
set local_script_path /Users/sean/dev/mfscripts
set remote_script_path /projects/scripts

# create and config remote_script_path ... if required
if [ catch { asset.namespace.create :namespace $remote_script_path } result ] {
	puts "Remote script path already exists"
	}

# asset content (the scripts that get run) permission setup
asset.namespace.acl.grant :namespace $remote_script_path :acl < :actor -type domain ivec :access < :namespace access :namespace execute :asset access :asset-content access > >

# NB: public:public user can't ever see an asset via path (due to projects-published view) so have to use ID
# FIXME - we probably don't care for these -> only usable by non-public actors
# --- upload/update
import -namespace "$remote_script_path" $local_script_path/project_create.tcl
import -namespace "$remote_script_path" $local_script_path/project_modify.tcl
import -namespace "$remote_script_path" $local_script_path/project_grant.tcl
import -namespace "$remote_script_path" $local_script_path/project_revoke.tcl
import -namespace "$remote_script_path" $local_script_path/project_destroy.tcl
import -namespace "$remote_script_path" $local_script_path/project_get.tcl
import -namespace "$remote_script_path" $local_script_path/project_describe.tcl
import -namespace "$remote_script_path" $local_script_path/project_list.tcl


# --- get the asset IDs
set tmp [asset.query :where "namespace='$remote_script_path' and name='project_create.tcl'"]
set id_project_create [xvalue "id" $tmp]

set tmp [asset.query :where "namespace='$remote_script_path' and name='project_modify.tcl'"]
set id_project_modify [xvalue "id" $tmp]

set tmp [asset.query :where "namespace='$remote_script_path' and name='project_grant.tcl'"]
set id_project_grant [xvalue "id" $tmp]

set tmp [asset.query :where "namespace='$remote_script_path' and name='project_revoke.tcl'"]
set id_project_revoke [xvalue "id" $tmp]

set tmp [asset.query :where "namespace='$remote_script_path' and name='project_destroy.tcl'"]
set id_project_destroy [xvalue "id" $tmp]

set tmp [asset.query :where "namespace='$remote_script_path' and name='project_get.tcl'"]
set id_project_get [xvalue "id" $tmp]

set tmp [asset.query :where "namespace='$remote_script_path' and name='project_describe.tcl'"]
set id_project_describe [xvalue "id" $tmp]

set tmp [asset.query :where "namespace='$remote_script_path' and name='project_list.tcl'"]
set id_project_list [xvalue "id" $tmp]


# --- update/install custom service calls
system.service.add :name "project.create" :replace-if-exists true \
    :description "Custom project creation."\
    :access ACCESS :object-meta-access ACCESS \
    :definition < :element -name name -type string :element -name store -type string :element -name quota -type string :element -name description -type string -min-occurs 0 -default None > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_project_create :arg -name name \[xvalue name \$args\] :arg -name store \[xvalue store \$args\] :arg -name quota \[xvalue quota \$args\] :arg -name description \[xvalue description \$args\] \]\]"

system.service.add :name "project.modify" :replace-if-exists true \
    :description "Modify project settings."\
    :access ACCESS :object-meta-access ACCESS \
    :definition < :element -name name -type string :element -name description -type string -min-occurs 0 -default \  :element -name quota -type string -min-occurs 0 -default \  > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_project_modify :arg -name name \[xvalue name \$args\] :arg -name description \[xvalue description \$args\] :arg -name quota \[xvalue quota \$args\] \]\]"

system.service.add :name "project.grant" :replace-if-exists true \
    :description "Grant project membership."\
    :access ACCESS :object-meta-access ACCESS \
    :definition < :element -name name -type string :element -name administer -type string -min-occurs 0 -default \  :element -name readwrite -type string -min-occurs 0 -default \  :element -name readonly -type string -min-occurs 0 -default \  > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_project_grant :arg -name name \[xvalue name \$args\] :arg -name administer \[xvalue administer \$args\] :arg -name readwrite \[xvalue readwrite \$args\] :arg -name readonly \[xvalue readonly \$args\] \]\]"

system.service.add :name "project.revoke" :replace-if-exists true \
    :description "Revoke project membership."\
    :access ACCESS :object-meta-access ACCESS \
    :definition < :element -name name -type string :element -name administer -type string -min-occurs 0 -default \  :element -name readwrite -type string -min-occurs 0 -default \  :element -name readonly -type string -min-occurs 0 -default \  > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_project_revoke :arg -name name \[xvalue name \$args\] :arg -name administer \[xvalue administer \$args\] :arg -name readwrite \[xvalue readwrite \$args\] :arg -name readonly \[xvalue readonly \$args\] \]\]"

system.service.add :name "project.destroy" :replace-if-exists true \
    :description "Destroy a project."\
    :access ACCESS :object-meta-access ACCESS \
    :definition < :element -name name -type string :element -name destroy-data -type boolean -min-occurs 0 -default false > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_project_destroy :arg -name name \[xvalue name \$args\] :arg -name destroy \[xvalue destroy-data \$args\] \]\]"

system.service.add :name "project.get" :replace-if-exists true \
    :description "Get project details."\
    :access ACCESS :object-meta-access ACCESS \
    :definition < :element -name name -type string -min-occurs 0 -default all :element -name show-members -type boolean -min-occurs 0 -default false > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_project_get :arg -name name \[xvalue name \$args\] :arg -name members \[xvalue show-members \$args\] \]\]"

system.service.add :name "project.describe" :replace-if-exists true \
    :description "Custom project description for the web portal."\
    :access ACCESS :object-meta-access ACCESS \
    :definition < :element -name name -type string > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_project_describe :arg -name name \[xvalue name \$args\] \]\]"

system.service.add :name "project.list" :replace-if-exists true \
    :description "List projects."\
    :access ACCESS :object-meta-access ACCESS \
    :definition < :element -name filter -type string -min-occurs 0 -default "*" > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_project_list :arg -name filter \[xvalue filter \$args\] \]\]"


# service call permissions for users
actor.grant :name ivec :type domain :perm < :access ACCESS :resource -type service project.get >

# these are granted via the admin role
# allow visibility of these to all - only those with auth user role can use though
#actor.grant :name ivec :type domain :perm < :access ACCESS :resource -type service project.grant >
#actor.grant :name ivec :type domain :perm < :access ACCESS :resource -type service project.revoke >


# doesn't work when running from mf_config ...
srefresh
