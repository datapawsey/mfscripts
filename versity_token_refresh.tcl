# for all (non-system) asset stores: insert new token
# NB: can check mediaflux-server.log for the puts output

set xml_list [asset.store.list :type versity]

set store_count [xcount "store/@id" $xml_list]

# get from ScoutAM call
set json_reply [rest.post :send json :response json :payload < :acct aaaaaa :pass xxxxxx > :url "https://localhost:6002/v1/security/login"]

set token [xvalue "response/response" $json_reply]
puts "new token: $token"

#set token "xyz"
#puts "# stores = $store_count"

for { set i 0 } { $i < $store_count } { incr i } {
	set store [xvalue "store\[$i\]/name" $xml_list]
    puts "inserting new token into store: $store"

    asset.store.arg.set :store $store :arg -name versity.api.auth.token $token
	}

