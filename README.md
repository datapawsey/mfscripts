# setup custom service calls for the Pawsey web portal

Deployment pattern is the same for each of the groups.
1. Edit the config_*.tcl file to change the local script path to point to where the child scripts are locally stored
2. Source the config_*.tcl file when logged in as system:manager 

Group 1 - custom service calls for the web portal

config_www_from_import.tcl
|- www_list.tcl
|- project_describe.tcl

Group 2 - project usage and activity audit and purge scheduled jobs

config_audit_usage.tcl
|- mf_audit_usage.tcl
|- mf_audit_purge.tcl
