# SDF - custom list service for the web portal
# SDF - 25/11/2014 - initial code
# SDF - 30/3/2015 - capping size at lower value as server has a fit if 100k+ assets
# SDF - 15/4/2015 - redo to handle pagination (assets + namespaces!)
# SDF - 1/2/2016 - added content status reporting
# SDF - 29/6/2016 - added asset name text filtering
# SDF - 5/7/2016 - added recursive namespace searching
# SDF - 26/7/2016 - added "direction" output field; which contains the value of the -to attribute of migrating assets 

# needs to be double escaped as "sean\\'s project" ... note the double escaping on the double escaping
# GAWD - regex + TCL + Java = the stuff of nightmares
regsub -all "'" $namespace "\\\\\\\\'" escaped_namespace

# NEW - subtle change in behaviour (mf v 4.4.034) now need like '.*xml.*' (ie java string regex) instead of like 'xml' 
#regsub -all {\*} $filter "" filter
#regsub -all {\?} $filter "" filter

# remove leading/trailing spaces so the default of " " has length = 0
# if filter contains * -> convert what this "means" into java style regex
# if filter has no * -> append and concat .* so behaviour is a find anywhere sub-string filter
set filter [string trim $filter]
if { [string length $filter] > 0 } {
	if [ regexp {\*} $filter matchresult ] {
		regsub -all {\*} $filter ".*" filter
	} else {
		set filter ".*$filter.*"
	}
}

# get total number of namespaces + assets
#set query1 [asset.namespace.describe :namespace "$namespace" :vacount true]
# avoid asset.namespace.describe ... it's SLOW
set query1 [asset.namespace.list :namespace "$namespace"]

# NEW - enable recursive namespace query
if {[string is true $recurse]} {
	set namespace_query "namespace>='$escaped_namespace'"
} else {
	set namespace_query "namespace='$escaped_namespace'"
}

# NEW - this was broken as default filter is NOT len = 0
if {[string match ".*" $filter]} {
# asset and namespace query (default filter)
	set query2 [asset.query :where "$namespace_query" :action count]
	set namespace_count [xcount "namespace/namespace" $query1]
} else {
# asset-only query
	set query2 [asset.query :where "$namespace_query and name like '$filter'" :action count]
	set namespace_count 0
}
set asset_count [xvalue "value" $query2]

# NEW - skip namespaces if recursive
if {[string is true $recurse]} {
	set namespace_count 0
}

# init the XML return document
set new_result [xnew]

# page size checks
if { $size < 1 } { set size 1 }
#if { $size > 100 } { set size 100 }

# compute $page_count
set page_count [ expr ( 1 + int(( $namespace_count + $asset_count - 1 ) / $size )) ]
set namespace_page_count [ expr ( 1 + int(( $namespace_count - 1 ) / $size )) ]

# page indexing checks
if { $page_count == 0 } { set page_count 1 }
if { $page < 1 } { set page 1 }
if { $page > $page_count } { set page $page_count }

# want number of namespaces (IF there are any) to page through before we start showing assets
if { $namespace_page_count == 0 } { set namespace_page_count 1 }
if { $namespace_count == 0 } { set namespace_page_count 0 }

# return input namespace in the result
set doc [xnew :parent < :name [xsafe $namespace] :namespaces [xsafe $namespace_count] :assets [xsafe $asset_count] :size [xsafe $size] :page [xsafe $page] :last [xsafe $page_count] :filter [xsafe $filter] :recurse [xsafe $recurse] > ]
set new_result [xadd $new_result $doc]

# DEBUG
#set new_result [xadd $new_result "<result><debug_namespace_count>$namespace_count</debug_namespace_count></result>"]

# --- namespace output
if { $page <= $namespace_page_count } { set todo_namespace 1 } else { set todo_namespace 0 } 

# number of results to return (max)
set todo_count $size

# if namespaces are to be returned ...
if { $todo_namespace == 1 } {

# I hate TCL (namespace/namespace[1] doesn't seem to work on asset.namespace.describe results - only on list)
#set namespace_id_list [xvalues namespace/namespace/@id $query1]
set namespace_idx_start [ expr ($page-1) * $size ]
set namespace_idx_stop [ expr $namespace_count - 1 ]
set namespace_idx $namespace_idx_start

# while still results to return and namespace bounds not exceeded
while { $todo_count > 0 && $namespace_idx <= $namespace_idx_stop } {

# get the namespace
	set ns [xvalue "namespace/namespace\[$namespace_idx\]" $query1]

# output
	set doc [xnew :namespace < :name [xsafe $ns] > ]
	set new_result [xadd $new_result $doc]

	incr namespace_idx
	incr todo_count -1
	}
}

# if still need more results (page size not filled) -> do assets
if { $todo_count > 0 } { 

# number of assets in the last "namespace" page
set asset_delta [expr $namespace_page_count*$size - $namespace_count]

# compute offset ....
# if < 0 => exit (shouldn't happen)
# if 0 -> OK
# if 1 -> set to 0 + delta
# if >1 -> dec by 1 -> x remainder by size + delta

set asset_page_start [expr $page - $namespace_page_count]

if { $asset_page_start > 0 } { set asset_idx [expr ($asset_page_start-1)*$size + $asset_delta + 1] } else { set asset_idx 1 }

# --- asset metadata query
if { [string length $filter] == 0 } {
	set query2 [asset.query :where "$namespace_query" :action get-values :xpath "id" -ename "id" :xpath "name" -ename "name" :xpath "content/type" -ename "type" :xpath "content/size" -ename "size" :xpath "mtime" -ename "mtime" :size $todo_count :idx $asset_idx :pipe-generate-result-xml true]
} else {
	set query2 [asset.query :where "$namespace_query and name like '$filter'" :action get-values :xpath "id" -ename "id" :xpath "name" -ename "name" :xpath "content/type" -ename "type" :xpath "content/size" -ename "size" :xpath "mtime" -ename "mtime" :size $todo_count :idx $asset_idx :pipe-generate-result-xml true]
}

# published element to insert
set doc [xnew :published "true"]

# for each asset 
set id_list [xvalues "asset/@id" $query2]
set i 0
foreach id $id_list {
# if published -> insert extra element
	set pub [asset.label.exists :id $id :label "PUBLISHED"]
	set pub_val [xvalue "exists" $pub]
	if { $pub_val == "true" } {
		set query2 [xadd $query2 $doc "/asset\[$i\]" ]
		}

# add the content status (online/offline)
	set status [asset.content.status :id $id]
	set state [xvalue "asset/state" $status]

# pull the migration direction attribute (if any)
	set direction [xvalue "asset/state/@to" $status]
	if { [string length $direction] == 0 } {
		set doc2 [xnew :state $state]
	} else {
		set doc2 [xnew :state $state :direction $direction]
	}

	set query2 [xadd $query2 $doc2 "/asset\[$i\]" ]

	incr i
	}

# TODO - can we remove the :cursor element??? doesn't seem to be an xdel to match xadd 

set new_result [xadd $new_result $query2]
}

return $new_result
