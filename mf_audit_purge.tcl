
# SDF - audit log dump and subsequent purge from DB

set path "/mnt/ivec01fs/backup/livearc/"
set name "livearc_audit_"

append name [clock format [clock seconds] -format {%Y-%m-%d_%H:%M:%S}]

# save
audit.export.csv :to "now - 4 week" :url file:$path$name
# purge
audit.purge :to "now - 4 week" 

