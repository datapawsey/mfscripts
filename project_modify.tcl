
# modify project settings
#set name "Data Team"
#set description ""
#set quota ""

if {[string length $description] > 1} {
# namespace describe can be extremely slow on production - switched to using role.namespace description
#	asset.namespace.description.set :namespace "/projects/$name" :description "$description"
	authorization.role.namespace.modify :namespace "$name" :description "$description"
	}

if {[string length $quota] > 1} {
#	asset.store.arg.set :store "$name" :arg -name file.max.size "$quota"
    asset.namespace.quota.set :namespace "/projects/$name" :quota < :allocation "$quota" > :replace true
}
