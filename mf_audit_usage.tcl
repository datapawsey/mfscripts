
# don't need date - it's embedded as time in the audit event
#set date [clock format [clock seconds] -format {%Y-%m-%d_%H:%M:%S}]

set result [asset.namespace.list :namespace /projects]

set project_count [xcount "namespace/namespace" $result]

for { set i 0 } { $i < $project_count } { incr i } {

# TODO - evaluation?
#	set current_health "unknown"

	set name [xvalue "namespace/namespace\[$i\]" $result]

# TODO - skip hidden/other (eg system)
# gracefully cope with failure (names don't match namespace and asset store)
	if [ catch { asset.store.describe :name $name } details ] {
		puts "Error: project $name has no matching asset store."
		continue
	}


# TODO - check for unmounted status

	set usage [xvalue "store/mount/size" $details]
# FIXME - strictly arg -name "file.max.size"
	set quota [xvalue "store/arg" $details]


#puts "$name : $usage : $quota"

#set tmp [ audit.event.create :type "asset.store.usage" :field -name "name" $name :field -name "usage" $usage :field -name "quota" $quota ]

#puts $tmp

if [ catch { audit.event.create :type "asset.store.usage" :field -name "name" $name :field -name "usage" $usage :field -name "quota" $quota } ] {
	puts "Failed to create event for $name on $date"
	}


}




