#!/usr/bin/env python3

import os
import glob
import datetime
from operator import itemgetter

# back directory location
dumpdir = '/Users/fle135/tmp/'
# keep max of 8 backup items
keepmax = 8
# most recent should be newer than 7 days
recent = 7


def cleanup(item_list, oldest, newest):

    count = len(item_list)
    msg = ""
    warn = 0

    if count < keepmax:
        msg += "Found less than %d items" % keepmax
        warn = 1
    else:
        msg += "Deleting old items: %d" % (count-keepmax)
        for i in range(keepmax, count):
            print("delete: ", item_list[i][1])
#            os.remove(item_list[i][1])

    if newest > recent:
        msg += "Most recent item is older than %s days" % recent
        warn = 1

# TODO - anything else ... eg significant size changes ...

    return warn, msg


if __name__ == '__main__':

    date_today = datetime.date.today()

# TODO - run scan of audit dump dir ... same as DB ... if required ...
#    list_audit = []
#    for name in glob.glob(dumpdir+"mflux_audit_*"):
#        print(name)
#        list_audit.append(name)

# run scan of db dump dir
# NB: expecting names of the form mflux_db_%Y-%m-%d_%H:%M:%S
# ref: https://support.pawsey.org.au/documentation/pages/viewpage.action?pageId=4065839#Whatdoesmfdeployactuallydo?-Automatedbackups
    list_db = []
    path_db_prefix = dumpdir+"mflux_db_*"
    path_db_len = len(path_db_prefix) - 1
    dt_min = 999999999
    dt_max = -1
    for name in glob.glob(path_db_prefix):

        date1 = datetime.datetime.strptime(name[path_db_len:path_db_len+10], "%Y-%m-%d").date()

        dt = date_today - date1
        if dt.days < dt_min:
            dt_min = dt.days
        if dt.days > dt_max:
            dt_max = dt.days
# TODO - could add filesize to tupple and have additional check for suspicious changes
        list_db.append((dt.days, name))


# process sorted list to cleanup and generate any messages
    warn, msg = cleanup(sorted(list_db, key=itemgetter(0)), dt_max, dt_min)
    if warn != 0:
        print("TODO - email admin the msg")

    print(msg)


