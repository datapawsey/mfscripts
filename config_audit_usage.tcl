
set local_script_path /Users/sean/work/livearc/scripts

# uploads files into mediaflux
import -namespace "/system/scripts" $local_script_path/mf_audit_usage.tcl
import -namespace "/system/scripts" $local_script_path/mf_audit_purge.tcl


# create (if required) the audit type
catch { audit.type.create :type "asset.store.usage" :description "Asset store usage at audit event timestamp" :field -name "name" < :type -type string :description "Asset store name" > :field -name "usage" < :type -type  long :description "The usage" > :field -name "quota" < :type -type long :description "The quota" > :enabled true }


# install/refresh scheduled job to create audit events with usage/quota
catch { schedule.job.destroy :job audit-usage }
schedule.job.create :name audit-usage :when < :every -unit hour 12 > :id "path=/system/scripts/mf_audit_usage.tcl"


# install the audit purge script and job schedule
catch { schedule.job.destroy :job audit-purge }
schedule.job.create :name audit-purge :when < :dow 1 > :id "path=/system/scripts/mf_audit_purge.tcl"
