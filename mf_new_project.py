#!/usr/bin/python

import os
import argparse
import getpass
import posixpath
import ConfigParser

import libs.mfclient as mfclient

project_domain_default = "ivec"
project_defaults = { 'rdsi':("/mnt/RDSI_GPFS01/cs01/RDSI/", "file-system"), 'gensci':("/mnt/livearc01fs/", "dmf-file-system"), 'test':("/home/sean/mflux_data", "file-system"), 'system-data':("/mnt/livearc_db/volatile/stores/data", "file-system") , 'test-gpfs':("/mnt/test_gpfs01/MFTest", "file-system")}

if __name__ == '__main__':

# server config (section heading) to use
	p = argparse.ArgumentParser(description='pshell help')
	p.add_argument('-c', dest='config', default='pawsey', help='the configuration name describing the remote server to connect to (eg pawsey)')
	args = p.parse_args()
	current = args.config
	config = ConfigParser.ConfigParser()
	config_filepath = os.path.expanduser("~/.mf_config")

# use config to set up mediaflux server to talk to
	try:
		config.read(config_filepath)
		server = config.get(current, 'server')
		protocol = config.get(current, 'protocol')
		port = config.get(current, 'port')

		mf_client = mfclient.mf_client(protocol=protocol, port=port, server=server, enforce_encrypted_login=False, debug=True)
	except Exception as e:
		print "Failed to initialize configuration [%s] with error: %s" % (current, e)
		exit(-1)

# enforce system:manager login 
	user = "manager"
	domain = "system"
	password = getpass.getpass("Enter %s:%s password: " % (domain, user))
	try:
		mf_client.login(domain, user, password)
	except Exception as e:
		print "Login failed in: %s" % str(e)
		exit(-1)

# MAIN - project creation code

# asset store name == project_name == role:namespace name
	project_name = raw_input("Short name of new project: ")
	project_namespace = posixpath.join("/projects/", project_name)

# NEW
	project_description = raw_input("Descriptive name of new project: ")

# --- get project authentication domain
	project_domain = raw_input("Project domain - hit enter for default [%s]" % project_domain_default)
	project_domain = project_domain or project_domain_default

# ---
	role_admin = project_name + ':administer'
	role_rw = project_name + ':readwrite'
	role_ro = project_name + ':readonly'


# build predefined filesystem keys
	fs_option = ""
	for key in project_defaults:
		fs_option += " " + key
	user_select = raw_input("Project filesystem (%s): " % fs_option)
	try:
		project_fs = project_defaults[user_select]
	except:
		print "No such project identifier"
		exit()

	storage_root = project_fs[0]
	storage_type = project_fs[1]

# backing filestore on SERVER is linux...
	storage_path = posixpath.join(storage_root, project_name)

# NEW - quota
	storage_quota = raw_input("Storage quota in Bytes: ")

# --- get user lists
	raw_admin = raw_input("Enter PI list: ")
	admin_list = raw_admin.split()

	raw_readwrite = raw_input("Enter readwrite list: ")
	rw_list = raw_readwrite.split()

	raw_readonly = raw_input("Enter readonly list: ")
	ro_list = raw_readonly.split()

# NEW - public access feature
	public_sharing = raw_input("Enable public sharing [y/n] ")
 

# TODO - mfclient calls to do the business ...

# args_init = [("where", "namespace >='%s'" % candidate), ("count", "true"), ("action", "sum") , ("xpath", "content/size") ]
# result = self.mf_client.run("asset.query", args_init)

# config store
	print "Creating asset store..."
	args = [("name", project_name), ("type", storage_type), ("path", storage_path)]
	result = mf_client.run("asset.store.create", args)

	args = [("namespace", project_namespace)]
	result = mf_client.run("asset.namespace.create", args)

	args = [("namespace", project_namespace), ("store", project_name)]
	result = mf_client.run("asset.namespace.store.set", args)


	print "Attaching quota..."
	args = [("store", project_name), ("arg name=\"file.max.size\"", storage_quota_bytes)]
	result = mf_client.run("asset.store.arg.set", args)


# config permissions layout
	print "Creating role-namespace..."
	args = [("namespace", project_name), ("description", project_description)]
	result = mf_client.run("authorization.role.namespace.create", args)


	print "Creating new roles..."
	args = [("role", role_admin)]
	result = mf_client.run("authorization.role.create", args)
	args = [("role", role_rw)]
	result = mf_client.run("authorization.role.create", args)
	args = [("role", role_ro)]
	result = mf_client.run("authorization.role.create", args)



	print "Grant ADMIN on role-namespace..."

# CURRENT - ugh - how do I form the args for this (nested XML) ????
# TODO
#	mf_execute("actor.grant :type role :name %s :perm < :access ADMINISTER :resource -type role:namespace %s >" % (role_admin, project_name))

	args = [("name", role_admin), ("type", "role"), ("role type role", "authorizing-user")]
	result = mf_client.run("actor.grant", args)

# TODO

	print "Grant ACLs for new roles..."
# NB: it seems you need BOTH destroy and administer privs on a namespace to destroy it ... go figure
# NB: use SET for the first call to WIPE the old ACLs ... then grant thereafter ...
	mf_execute("asset.namespace.acl.set :namespace %s :acl < :actor -type role %s :access < :namespace access :namespace execute :namespace create :namespace destroy :namespace administer :asset access :asset create :asset modify :asset destroy :asset-content access :asset-content modify > >" % (project_namespace, role_admin))

# TODO - readwrite should be able to destroy namespaces without admin privs (which also enables role namespace manipulation) ... FIXME for arcitecta
	mf_execute("asset.namespace.acl.grant :namespace %s :acl < :actor -type role %s :access < :namespace access :namespace execute :namespace create :namespace destroy :asset access :asset create :asset modify :asset destroy :asset-content access :asset-content modify > >" % (project_namespace, role_rw))
	mf_execute("asset.namespace.acl.grant :namespace %s :acl < :actor -type role %s :access < :namespace access :namespace execute :asset access :asset-content access > >" % (project_namespace, role_ro))

	print "Granting ADMIN rights to user..."
	mf_execute("authentication.user.grant :domain %s :user %s :role -type role %s" % (domain, user, role_admin))

	if public_sharing:
		print "Enabling public sharing..."
		mf_execute("actor.grant :name public :type domain :role -type role %s" % role_ro)
	print "Ok"


