
# inexpensive list of all projects with minimum details 
# intended for dashboard overview

proc project_details_get {name} {

if [ catch { asset.store.describe :name $name } xml_info ] {
# empty doc -> skip display if there is no attached store (not a Pawsey-style project)
	set xdoc_result [xnew] 

} else {

#	set namespace "/projects/$name"
# TODO - make optional if slows things down too much
#	set xml_namespace [asset.namespace.describe :namespace $namespace :list-contents false]
# CURRENT - replacing ALL namespace queries with role namespace queries

	set xml_rolens [authorization.role.namespace.describe :namespace $name]
	set description [xvalue namespace/description $xml_rolens]

	set xdoc_result [xnew :project]
	set xdoc_result [xadd $xdoc_result [xnew :name [xsafe $name]] /project]
	set xdoc_result [xadd $xdoc_result [xnew :description [xsafe $description]] /project]

# store unmounted?
	set mounted [xvalue store/automount/@mounted $xml_info]

	if {[string equal $mounted "true"]} {

		set quota [xvalue store/mount/max-size $xml_info]
		set usage [xvalue store/mount/size $xml_info]
		set quota_human [xvalue store/mount/max-size/@h $xml_info]
		set usage_human [xvalue store/mount/size/@h $xml_info]

# TODO - if length of human usage/quota is 0 -> probably unquoted -> display unknown or similar
		if {[string length $quota_human] < 1} {
			set quota_human "Unlimited"
		}
		if {[string length $usage_human] < 1} {
			set usage_human "Unknown"
		}

	set xdoc_quota [xnew :quota -bytes \"$quota\" [xsafe $quota_human] :used -bytes \"$usage\" [xsafe $usage_human] :mounted [xsafe $mounted]]

	} else {

# FIXME - strictly, should check that name = file.max.size ... but we're not using any other arg types at present
	set quota [xvalue store/arg $xml_info]

	set xdoc_quota [xnew :quota \"$quota\" :used "unknown" :mounted [xsafe $mounted]]
	}

set xdoc_result [xadd $xdoc_result $xdoc_quota /project]
}

#puts $xdoc_result

return $xdoc_result
}


# --- MAIN

set xdoc_total [xnew]

#set result [asset.namespace.list :namespace /projects]
#set project_count [xcount "namespace/namespace" $result]
#
#for { set i 0 } { $i < $project_count } { incr i } {
#	set name [xvalue "namespace/namespace\[$i\]" $result]
#	if { [string match $filter $name] } {
#		set xdoc_this [project_details_get $name]
#		set xdoc_total [xadd $xdoc_total $xdoc_this]
#	}
#}
#set filter "*"

set result [authorization.role.namespace.list]
set count [xcount "namespace" $result]

#puts "count = $count"

for { set i 0 } { $i < $count } { incr i } {

	set name [xvalue "namespace\[$i\]/@name" $result]
#	puts "name = $name"
	if { [string match $filter $name] } {
		set xdoc_this [project_details_get $name]
		set xdoc_total [xadd $xdoc_total $xdoc_this]
	}

}

puts $xdoc_total

return $xdoc_total
