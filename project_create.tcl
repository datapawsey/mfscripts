# project creation script
# inputs: $name $quota and $store

set namespace "/projects/$name"

# if we get an error - old project scraps may be left around -> back out
if { [ catch {
	asset.namespace.create :namespace $namespace :store $store
    asset.namespace.quota.set :namespace $namespace :quota < :allocation $quota >
} result ] } {
	return -code error {"Error: bad namespace, store, or quota"}
} 

if { [ catch {
	authorization.role.namespace.create :namespace $name :description $description
} result ] } {
	return -code error {"Error: project role namespace of that name already exists"}
}

# these should not exist
authorization.role.create :role "$name:administer"
authorization.role.create :role "$name:readwrite"
authorization.role.create :role "$name:readonly"

# admin is special 
# admin on the role namespace
actor.grant :type role :name "$name:administer" :perm < :resource -type role:namespace $name :access ADMINISTER >
# NEW - ACCESS on the namespace is also required in order to see the membership (MF 4.7.017 change)
actor.grant :type role :name "$name:administer" :perm < :resource -type role:namespace $name :access ACCESS >
actor.grant :type role :name "$name:readwrite" :perm < :resource -type role:namespace $name :access ACCESS >
actor.grant :type role :name "$name:readonly" :perm < :resource -type role:namespace $name :access ACCESS >

# service call permissions
actor.grant :name "$name:administer" :type role :perm < :access ADMINISTER :resource -type service actor.grant >
actor.grant :name "$name:administer" :type role :perm < :access ADMINISTER :resource -type service actor.revoke >
actor.grant :name "$name:administer" :type role :perm < :access ACCESS :resource -type service project.grant >
actor.grant :name "$name:administer" :type role :perm < :access ACCESS :resource -type service project.revoke >

# role based namespace acls
# NB: no difference between admin and readwrite at this level
# NB: administer on a namespace lets you change things like: description AND quota on a namespace
asset.namespace.acl.set :namespace $namespace :acl < :actor -type role "$name:administer" :access < :namespace access :namespace execute :namespace create :namespace destroy :namespace modify :asset access :asset create :asset modify :asset destroy :asset-content access :asset-content modify > >
asset.namespace.acl.grant :namespace $namespace :acl < :actor -type role "$name:readwrite" :access < :namespace access :namespace execute :namespace create :namespace destroy :namespace modify :asset access :asset create :asset modify :asset destroy :asset-content access :asset-content modify > >
asset.namespace.acl.grant :namespace $namespace :acl < :actor -type role "$name:readonly" :access < :namespace access :namespace execute :asset access :asset-content access > >


