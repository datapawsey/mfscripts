
# SDF - database dump script suitable for periodic backups

set path "/opt/backups/"
set name "mflux_db_"

append name [clock format [clock seconds] -format {%Y-%m-%d_%H:%M:%S}]

server.database.backup :url file://$path$name

