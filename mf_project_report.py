#!/usr/bin/env python3

import os
import re
import getpass
import mfclient

user = "manager"
domain = "system"
# generated with: secure.identity.token.create :role -type role "system-administrator" :role -type role "read-only" :max-token-length 32

if __name__ == "__main__":
    try:
        mf_client = mfclient.mf_client('https', '443', 'data.pawsey.org.au', domain=domain)
        mf_client.login(token=os.environ['MFLUX_REPORTING_TOKEN'])
    except Exception as e:
        print(str(e))
        exit(-1)

# institutional mappings
    hash_institution = { "adelaide":"Uni of Adelaide", "adfa":"ADFA", "aims":"AIMS", "anu":"ANU", "chalmers":"Chalmers Uni", "csiro":"CSIRO", "curtin":"Curtin Uni", "deakin":"Deakin Uni", "dmp":"Dept Mines", "dpaw":"Dept Parks and Wildlife", "dpird":"Dept Primary Industry", "dwer":"Dept Water", "ecu":"ECU", "form":"FORM", "icrar":"ICRAR", "landgate":"Landgate", "monash":"Monash Uni", "murdoch":"Murdoch Uni", "pawsey":"Pawsey Centre", "perkins":"Harry Perkins Inst", "rmit":"RMIT", "sydney":"Uni of Sydney", "telethonkids":"Telethon Kids Inst", "transport":"Dept of Transport", "unimelb":"Uni of Melbourne", "unsw":"Uni of NSW", "uow":"Uni of Wollongong", "uts":"Uni of Technology Sydney", "uwa.edu":"UWA", "health":"Dept of Health" }

# HEADER
    print("\nproject, owner, institution, created, store, usage (TB), quota (TB)")

# iterate over all projects by name
    reply = mf_client.aterm_run('asset.namespace.list :namespace /projects')
    elem_list = reply.findall('.//namespace/namespace')
    count_group = {}
    usage_group = {}
    inactive_group = {}
    for elem in elem_list:
        project = elem.text
        try:
            institution = None
            owner = None
            quota = 0.0
            usage = 0.0
            store = None
            reply = mf_client.aterm_run('project.describe :name %s' % project)
# NEW - requiring application owner email to be in the description of the role namespace
# NB: only system:admin can see this info currently
            elem = reply.find('.//description')
            if elem is not None:
                if elem.text is not None:
# full PI email (owner=xxx@abc.au)
                    if len(elem.text) > 6:
                        owner = elem.text[6:]
# determine institution association
                    match = re.search("@\S+", elem.text)
                    if match is not None:
                        domain = match.group(0)[1:]
                        institution = domain.split('.')[0]
# table of substitutions
                        for key in hash_institution.keys():
                            if key in domain:
                                institution = hash_institution[key]
# mark as "inactive project" if no institution found
            if institution is None:
                inactive_group[project] = 1
                continue
# namespace quota 
            try:
                xml_namespace = mf_client.aterm_run('asset.namespace.describe :namespace /projects/%s' % project)
# NB: this is the only place to get ctime 
                elem = xml_namespace.find('.//ctime')
                ctime = (elem.text).split()[0]
                elem = xml_namespace.find('.//quota/used')
                usage = float(elem.text) / 1000000000000.0
                elem = xml_namespace.find('.//quota/allocation')
                quota = float(elem.text) / 1000000000000.0
                elem = xml_namespace.find('.//store')
                store = (elem.text).split()[0]
# if there is an error -> asssume new style (ie missing data from above)
            except Exception as e:
                pass
                print(" >>> Warning: %s" % str(e))

# TODO - output grouped by institution
            if institution in count_group.keys():
                count_group[institution] += 1
                usage_group[institution] += float(usage)
            else:
                count_group[institution] = 1
                usage_group[institution] = float(usage)

# CSV project output
            print("%s, %s, %s, %s, %s, %.2f, %.1f" % (project, owner, institution, ctime, store, usage, quota))

        except Exception as e:
            print(" >>> Error")
            print(str(e))
            pass

# CSV institution grouped summary
    print("\ninstitution, count, usage")
    for institution in count_group.keys():
        try:
            print("%s, %d, %d" % (institution, count_group[institution], usage_group[institution]))
        except Exception as e:
            print(" >>> Error: %s" % str(e))

# TODO - should show creation date for these ...
    print("\nInactive or internal projects (empty role namespace description)")
    for project in inactive_group.keys():
        print(project)
