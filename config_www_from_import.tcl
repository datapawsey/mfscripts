
set local_script_path /Users/sean/dev/mfscripts
set remote_script_path /projects/scripts

# TODO - create and config remote_script_path ... if required
if [ catch { asset.namespace.create :namespace $remote_script_path } result ] {
	puts "already exists"
	}

# asset content (the scripts that get run) permission setup
asset.namespace.acl.grant :namespace $remote_script_path :acl < :actor -type domain ivec :access < :namespace access :namespace execute :asset access :asset-content access > >
asset.namespace.acl.grant :namespace $remote_script_path :acl < :actor -type domain public :access < :namespace access :namespace execute :asset access :asset-content access > >

# NB: public:public user can't ever see an asset via path (due to projects-published view) so have to use ID

# --- upload/update
# mf_configure - does the upload instead
import -namespace "$remote_script_path" $local_script_path/www_list.tcl
import -namespace "$remote_script_path" $local_script_path/project_describe.tcl

# --- get the asset IDs
set tmp [asset.query :where "namespace='$remote_script_path' and name='www_list.tcl'"]
set id_www_list [xvalue "id" $tmp]

set tmp [asset.query :where "namespace='$remote_script_path' and name='project_describe.tcl'"]
set id_project_describe [xvalue "id" $tmp]

# --- publish so the asset is visible in projects-published view
asset.label.add :id $id_www_list :label "PUBLISHED"
asset.label.add :id $id_project_describe :label "PUBLISHED"

# --- update/install custom service calls
system.service.add :name "www.list" :replace-if-exists true \
    :description "Custom namespace and asset listing for the web portal."\
    :access ACCESS :object-meta-access ACCESS :object-data-access ACCESS \
    :definition < :element -name namespace -type string :element -name page -type long -min-occurs 0 -default 1 :element -name size -type long -min-occurs 0 -default 20 :element -name filter -type string -min-occurs 0 -default \" \" :element -name recurse -type boolean -min-occurs 0 -default false > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_www_list :arg -name namespace \[xvalue namespace \$args\] :arg -name page \[xvalue page \$args\] :arg -name size \[xvalue size \$args\] :arg -name filter \[xvalue filter \$args\] :arg -name recurse \[xvalue recurse \$args\]\]\]"

system.service.add :name "project.describe" :replace-if-exists true \
    :description "Custom project description for the web portal."\
    :access ACCESS :object-meta-access ACCESS \
    :definition < :element -name name -type string > \
    :execute "return \[xvalue result \[asset.script.execute :id $id_project_describe :arg -name name \[xvalue name \$args\] \]\]"

# service call permission setup - might only be required for public
# TODO - same again for project.describe
# this is all required if public:public is not configured
actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.script.execute >
actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.namespace.list >
actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.namespace.exists >
actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.query >
actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.label.exists >
actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.content.status >
actor.grant :name public :type domain :perm < :access ACCESS :resource -type service www.list >

# doesn't work when running from mf_config ...
#srefresh
