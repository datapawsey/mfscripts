
# project creation script

set namespace "/projects/$name"

# TODO - get default store -> use this for backing path (fail if not set or set to db)
# maybe not - default store is what mediaflux uses for some of its stuff so we might need to be careful - plus the whole iron/clay thing
#set tmp [asset.store.describe :tag $store_tag]
#set base_path [xvalue "store/mount/path" $tmp]
#puts $base_path

# CURRENT - use the supplied store as info (file path/type) and create new
# this aligns with the current configuration and so will cause the least problems rolling out
# a possibility is to make it so it uses the store rather than creating a new one based on a flag

#set xml_store [asset.store.describe :name $store]
#set base_path [xvalue store/path $xml_store]
#set base_type [xvalue store/type $xml_store]
# CURRENT - rewrite to use the app "stores" metadata under the /projects namespace
#set xml_info [asset.namespace.application.settings.get :namespace /projects :app stores]
#set base_path [xvalue settings/$store/path $xml_info]
#set base_type [xvalue settings/$store/type $xml_info]
#
#set new_store_path "$base_path/$name"

#if { [ catch {
#	asset.store.create :name $name :path $new_store_path :type $base_type
#} result ] } {
#	return -code error {"Error: failed to create new store=$name based on template store=$store"}
#} 

# if we get an error - old project scraps may be left around -> back out
if { [ catch {
#	asset.namespace.create :namespace $namespace :store $name
#	asset.store.arg.set :store $name :arg -name file.max.size $quota
	asset.namespace.create :namespace $namespace :store $store :quota < :allocation $quota >
} result ] } {
	return -code error {"Error: bad namespace, store, or quota"}
} 

if { [ catch {
	authorization.role.namespace.create :namespace $name :description $description
} result ] } {
	return -code error {"Error: project role namespace of that name already exists"}
}

# these should not exist
authorization.role.create :role "$name:administer"
authorization.role.create :role "$name:readwrite"
authorization.role.create :role "$name:readonly"

# admin is special 
# admin on the role namespace
actor.grant :type role :name "$name:administer" :perm < :resource -type role:namespace $name :access ADMINISTER >
# service call permissions
actor.grant :name "$name:administer" :type role :perm < :access ADMINISTER :resource -type service actor.grant >
actor.grant :name "$name:administer" :type role :perm < :access ADMINISTER :resource -type service actor.revoke >
actor.grant :name "$name:administer" :type role :perm < :access ACCESS :resource -type service project.grant >
actor.grant :name "$name:administer" :type role :perm < :access ACCESS :resource -type service project.revoke >

# role based namespace acls
# NB: no difference between admin and readwrite at this level
# NB: administer on a namespace lets you change things like: description AND quota on a namespace
asset.namespace.acl.set :namespace $namespace :acl < :actor -type role "$name:administer" :access < :namespace access :namespace execute :namespace create :namespace destroy :namespace modify :asset access :asset create :asset modify :asset destroy :asset-content access :asset-content modify > >
asset.namespace.acl.grant :namespace $namespace :acl < :actor -type role "$name:readwrite" :access < :namespace access :namespace execute :namespace create :namespace destroy :namespace modify :asset access :asset create :asset modify :asset destroy :asset-content access :asset-content modify > >
asset.namespace.acl.grant :namespace $namespace :acl < :actor -type role "$name:readonly" :access < :namespace access :namespace execute :asset access :asset-content access > >


